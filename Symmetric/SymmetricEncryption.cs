﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Symmetric
{
    public class SymmetricEncryptionHandler
    {
        /// <summary>
        /// Key
        /// </summary>
        public byte[] Key { get; set; }

        /// <summary>
        /// IV
        /// </summary>
        public byte[] IV { get; set; }

        public int KeyAndBlockSize { get; set; }

        public byte[] GenerateRandomNumber(int length)
        {
            using (var randomNumberGenerator = new RNGCryptoServiceProvider())
            {
                var randomNumber = new byte[length];
                randomNumberGenerator.GetBytes(randomNumber);

                return randomNumber;
            }
        }


        //AES(CSP) (128 bit)  
        //AES(CSP) (256 bit) 
        //AES(Managed) (128 bit) 
        //AES(Managed) (256 bit) 
        //Rijndael(Managed) (128 bit) 
        //Rijndael(Managed) (256 bit) 
        //DES(CSP) (56 bit) 
        //3DES(CSP) (168 bit)

        /// <summary>
        /// Return Encrypt
        /// </summary>
        /// <param name="cryptographyStyle">
        /// 0:AES(CSP), 1:AES(Managed), 2:Rijndael(Managed) 3:DES 4:TripleDES
        /// </param>
        /// <returns></returns>
        public byte[] Encrypt(int cryptographyStyle, byte[] dataToEncrypt)
        {
            byte[] encrypt = null;
            switch (cryptographyStyle)
            {
                case 0:
                    encrypt = AESCSPEncrypt(
                        dataToEncrypt, 
                        this.Key, 
                        this.IV
                        );
                    break;
                case 1:
                    encrypt = AESManagedEncrypt(
                        dataToEncrypt,
                        this.Key,
                        this.IV
                        );
                    break;
                case 2:
                    encrypt = RijndaelManagedEncrypt(
                         dataToEncrypt,
                         this.Key,
                         this.IV
                         );
                    break;
                case 3:
                    encrypt = DESEncrypt(
                         dataToEncrypt,
                         this.Key,
                         this.IV
                         );
                    break;
                case 4:
                    encrypt = TripleDESEncrypt(
                         dataToEncrypt,
                         this.Key,
                         this.IV
                        );
                    break;
                default:
                    encrypt = AESCSPEncrypt(
                        dataToEncrypt,
                        this.Key,
                        this.IV
                        );
                    break;
            }
            return encrypt;
        }

        /// <summary>
        /// Return Decrypt
        /// </summary>
        /// <param name="cryptographyStyle">
        /// 0:AES(CSP), 1:AES(Managed), 2:Rijndael(Managed) 3:DES 4:TripleDES
        /// </param>
        /// <returns></returns>
        public byte[] Decrypt(int cryptographyStyle, byte[] dataToDecrypt)
        {
            byte[] decrypt = null;
            switch (cryptographyStyle)
            {
                case 0:
                    decrypt = AESCSPDecrypt(
                        dataToDecrypt,
                        this.Key,
                        this.IV
                        );
                    break;
                case 1:
                    decrypt = AESManagedEncrypt(
                        dataToDecrypt,
                        this.Key,
                        this.IV
                        );
                    break;
                case 2:
                    decrypt = RijndaelManagedDecrypt(
                        dataToDecrypt,
                        this.Key,
                        this.IV
                        );
                    break;
                case 3:
                    decrypt = DESDecrypt(
                        dataToDecrypt,
                        this.Key,
                        this.IV
                        );
                    break;
                case 4:
                    decrypt = TripleDESDecrypt(
                        dataToDecrypt,
                        this.Key,
                        this.IV
                        );
                    break;
                default:
                    decrypt = AESCSPDecrypt(
                        dataToDecrypt,
                        this.Key,
                        this.IV
                        );
                    break;
            }
            return decrypt;

        }
        private byte[] AESCSPEncrypt(byte[] dataToEncrypt, byte[] key, byte[] iv)
        {
            using (var des = new AesCryptoServiceProvider())
            {
                des.Mode = CipherMode.CBC;
                des.Padding = PaddingMode.PKCS7;

                des.Key = key;
                des.IV = iv;

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(),
                        CryptoStreamMode.Write);

                    cryptoStream.Write(dataToEncrypt, 0, dataToEncrypt.Length);
                    cryptoStream.FlushFinalBlock();

                    return memoryStream.ToArray();
                }
            }
        }

        public byte[] AESCSPDecrypt(byte[] dataToDecrypt, byte[] key, byte[] iv)
        {
            using (var des = new AesCryptoServiceProvider())
            {
                des.Mode = CipherMode.CBC;
                des.Padding = PaddingMode.PKCS7;

                des.Key = key;
                des.IV = iv;

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, des.CreateDecryptor(),
                        CryptoStreamMode.Write);

                    cryptoStream.Write(dataToDecrypt, 0, dataToDecrypt.Length);
                    cryptoStream.FlushFinalBlock();

                    var decryptBytes = memoryStream.ToArray();

                    return decryptBytes;
                }
            }
        }

        private byte[] AESManagedEncrypt(byte[] dataToEncrypt, byte[] key, byte[] iv)
        {
            using (var des = new AesManaged())
            {
                des.Mode = CipherMode.CBC;
                des.Padding = PaddingMode.PKCS7;

                des.Key = key;
                des.IV = iv;

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(),
                        CryptoStreamMode.Write);

                    cryptoStream.Write(dataToEncrypt, 0, dataToEncrypt.Length);
                    cryptoStream.FlushFinalBlock();

                    return memoryStream.ToArray();
                }
            }
        }

        public byte[] AESManagedDecrypt(byte[] dataToDecrypt, byte[] key, byte[] iv)
        {
            using (var des = new AesManaged())
            {
                des.Mode = CipherMode.CBC;
                des.Padding = PaddingMode.PKCS7;

                des.Key = key;
                des.IV = iv;

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, des.CreateDecryptor(),
                        CryptoStreamMode.Write);

                    cryptoStream.Write(dataToDecrypt, 0, dataToDecrypt.Length);
                    cryptoStream.FlushFinalBlock();

                    var decryptBytes = memoryStream.ToArray();

                    return decryptBytes;
                }
            }
        }

        private byte[] RijndaelManagedEncrypt(byte[] dataToEncrypt, byte[] key, byte[] iv)
        {
            using (var rij = new RijndaelManaged())
            {
                rij.Mode = CipherMode.CBC;
                rij.Padding = PaddingMode.PKCS7;

                rij.Key = key;
                rij.IV = iv;

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, rij.CreateEncryptor(),
                        CryptoStreamMode.Write);

                    cryptoStream.Write(dataToEncrypt, 0, dataToEncrypt.Length);
                    cryptoStream.FlushFinalBlock();

                    return memoryStream.ToArray();
                }
            }
        }

        public byte[] RijndaelManagedDecrypt(byte[] dataToDecrypt, byte[] key, byte[] iv)
        {
            using (var rij = new RijndaelManaged())
            {
                rij.Mode = CipherMode.CBC;
                rij.Padding = PaddingMode.PKCS7;

                rij.Key = key;
                rij.IV = iv;

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, rij.CreateDecryptor(),
                        CryptoStreamMode.Write);

                    cryptoStream.Write(dataToDecrypt, 0, dataToDecrypt.Length);
                    cryptoStream.FlushFinalBlock();

                    var decryptBytes = memoryStream.ToArray();

                    return decryptBytes;
                }
            }
        }

        private byte[] DESEncrypt(byte[] dataToEncrypt, byte[] key, byte[] iv)
        {
            using (var des = new DESCryptoServiceProvider())
            {
                des.Mode = CipherMode.CBC;
                des.Padding = PaddingMode.PKCS7;

                des.Key = key;
                des.IV = iv;

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(),
                        CryptoStreamMode.Write);

                    cryptoStream.Write(dataToEncrypt, 0, dataToEncrypt.Length);
                    cryptoStream.FlushFinalBlock();

                    return memoryStream.ToArray();
                }
            }
        }
        public  byte[] DESDecrypt(byte[] dataToDecrypt, byte[] key, byte[] iv)
        {
            using (var des = new DESCryptoServiceProvider())
            {
                des.Mode = CipherMode.CBC;
                des.Padding = PaddingMode.PKCS7;

                des.Key = key;
                des.IV = iv;

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, des.CreateDecryptor(),
                        CryptoStreamMode.Write);

                    cryptoStream.Write(dataToDecrypt, 0, dataToDecrypt.Length);
                    cryptoStream.FlushFinalBlock();

                    var decryptBytes = memoryStream.ToArray();

                    return decryptBytes;
                }
            }
        }

        private byte[] TripleDESEncrypt(byte[] dataToEncrypt, byte[] key, byte[] iv)
        {
            using (var des = new TripleDESCryptoServiceProvider())
            {
                des.Mode = CipherMode.CBC;
                des.Padding = PaddingMode.PKCS7;

                des.Key = key;
                des.IV = iv;

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(),
                        CryptoStreamMode.Write);

                    cryptoStream.Write(dataToEncrypt, 0, dataToEncrypt.Length);
                    cryptoStream.FlushFinalBlock();

                    return memoryStream.ToArray();
                }
            }
        }

        public byte[] TripleDESDecrypt(byte[] dataToDecrypt, byte[] key, byte[] iv)
        {
            using (var des = new TripleDESCryptoServiceProvider())
            {
                des.Mode = CipherMode.CBC;
                des.Padding = PaddingMode.PKCS7;

                des.Key = key;
                des.IV = iv;

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, des.CreateDecryptor(),
                        CryptoStreamMode.Write);

                    cryptoStream.Write(dataToDecrypt, 0, dataToDecrypt.Length);
                    cryptoStream.FlushFinalBlock();

                    var decryptBytes = memoryStream.ToArray();

                    return decryptBytes;
                }
            }
        }



    }
}
