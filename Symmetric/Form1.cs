﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Symmetric
{
    public partial class Form1 : Form
    {
        private SymmetricEncryptionHandler _symmetricEncryptionHandler;

        private byte[] encrypted;
        private byte[] decrypted;

        public Form1()
        {
            InitializeComponent();
            //initialize comboBox1 to crypto style
            this.comboBox1.SelectedIndex = 0;

            _symmetricEncryptionHandler = new SymmetricEncryptionHandler();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //var key = GenerateRandomNumber(8);
            //var iv = GenerateRandomNumber(8);

            //System.Security.Cryptography.AesCryptoServiceProvider
            //System.Security.Cryptography.AesManaged

            //System.Security.Cryptography.RijndaelManaged
            var rijindae = new System.Security.Cryptography.RijndaelManaged();

            //DESCryptoServiceProvider
            //System.Security.Cryptography.DESCryptoServiceProvider
            //TripleDESCryptoServiceProvider

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Generate Key and IV
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            int keyLength = 0;
            int ivLength = 0;

            //0: AES(CSP)(128 bit)
            //1: AES(CSP)(256 bit)
            //2: AES(Managed)(128 bit)
            //3: AES(Managed)(256 bit)
            //4: Rijndael(Managed)(128 bit)
            //5: Rijndael(Managed)(256 bit)
            //6: DES(CSP)(64 bit)
            //7: 3DES(CSP)(168 bit)


            switch (this.comboBox1.SelectedIndex)
            {
                case 0:
                case 2:
                case 4:
                    keyLength = 128 / 8;
                    ivLength = 128 / 8;
                    break;
                case 1:
                case 3:
                case 5:
                    keyLength = 256 / 8;
                    ivLength = 16;
                    break;
                case 6:
                    keyLength = 8;
                    ivLength = 8;
                    break;
                case 7:
                    keyLength = 192 / 8;
                    ivLength = 8;
                    break;
                default:
                    break;
            }


            _symmetricEncryptionHandler.Key = _symmetricEncryptionHandler.GenerateRandomNumber(keyLength);
            _symmetricEncryptionHandler.IV = _symmetricEncryptionHandler.GenerateRandomNumber(ivLength);
            //view key and iv
            this.textBox1.Text = Convert.ToBase64String(_symmetricEncryptionHandler.Key);
            this.textBox2.Text = Convert.ToBase64String(_symmetricEncryptionHandler.IV);
        }

        /// <summary>
        /// Encrypt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            //Encrypt style
            //0: AES(CSP)(128 bit)
            //1: AES(CSP)(256 bit)
            //2: AES(Managed)(128 bit)
            //3: AES(Managed)(256 bit)
            //4: Rijndael(Managed)(128 bit)
            //5: Rijndael(Managed)(256 bit)
            //6: DES(CSP)(56 bit)
            //7: 3DES(CSP)(168 bit)
            int encryptStyle = 0;
            switch (this.comboBox1.SelectedIndex)
            {
                case 0:
                    encryptStyle = 0;
                    break;
                case 1:
                    encryptStyle = 1;
                    break;
                case 4:
                case 5:
                    encryptStyle = 2;
                    break;
                case 6:
                    encryptStyle = 3;
                    break;
                case 7:
                    encryptStyle = 4;
                    break;
                default:
                    break;
            }
            //view hex efter Encrypt
            Stopwatch sw = new Stopwatch();

            sw.Start();
            decrypted = _symmetricEncryptionHandler.Encrypt(
                    encryptStyle,
                    Encoding.UTF8.GetBytes(this.textBox3.Text.Trim())
                    );
            sw.Stop();

            //view time
            double ticks = sw.ElapsedTicks;
            double seconds = ticks / Stopwatch.Frequency;
            this.label6.Text = string.Format("Time / message at encryption: {0} seconds", seconds.ToString());

            //origin text to HEX
            var hexString = BitConverter.ToString(Encoding.Default.GetBytes(this.textBox3.Text.Trim()));
            this.textBox4.Text = hexString;

            //Cipher ASCII
            this.textBox5.Text = Convert.ToBase64String(decrypted);
            //cipher HEX
            this.textBox6.Text = BitConverter.ToString(decrypted);

        }

        /// <summary>
        /// Decrypt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            int decryptStyle = 0;
            switch (this.comboBox1.SelectedIndex)
            {
                case 0:
                    decryptStyle = 0;
                    break;
                case 1:
                    decryptStyle = 1;
                    break;
                case 4:
                case 5:
                    decryptStyle = 2;
                    break;
                case 6:
                    decryptStyle = 3;
                    break;
                case 7:
                    decryptStyle = 4;
                    break;
                default:
                    break;
            }


            Stopwatch sw = new Stopwatch();

            sw.Start();
            encrypted = _symmetricEncryptionHandler.Decrypt(decryptStyle, decrypted);
            sw.Stop();

            //view time
            double ticks = sw.ElapsedTicks;
            double seconds = ticks / Stopwatch.Frequency;
            this.label7.Text = string.Format("Time / message at decryption: {0} seconds", seconds.ToString());

            this.textBox3.Text = Encoding.UTF8.GetString(encrypted);
        }
    }
}
